import axios from "axios";

export default axios.create({
  baseURL: "http://localhost/tutorials-be/api",
  headers: {
    "Content-type": "application/json"
  }
});
